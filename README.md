# Online Order

**Notes:**
1. online-order-client is also available running at http://shrouded-retreat-40978.herokuapp.com
2. online-order-service is also available running at https://fast-island-38275.herokuapp.com
3. Heroku server goes to sleep mode, when not used for some time, so the first request may take few more seconds than usual.

**Credentials:**
1. Username: pratik702
2. Password: josh

**Prerequisites:**
1. Jdk 1.8 should be installed with command line access.
2. Maven should be installed with command line access.

**Steps to setup:**
1. Clone this project.
2. Open command prompt and traverse to project directory online-order.
3. Run the following command:
        mvn clean install package
4. Traverse to online.order/target.
5. Run the following command:
        java -jar online-order-0.0.1-SNAPSHOT.jar

**Provided APIs:**
1. Test: "/"
2. Login: "/login"
3. Signup: "/sign-up"
4. Get list of menus: "/order/menus
5. Place new order: "/order/place-order"
6. Get Dashboard data: "/order/dashboard"
7. Get all placed orders: "/order/orderbooks"

**Database Details:**
1. Database: H2
2. Data will be deleted after server restart.
3. Schema is defined in schema.sql and Seed data is defined in data.sql.
4. DB Console: http://localhost:8080/h2-console. Not available in production mode.

