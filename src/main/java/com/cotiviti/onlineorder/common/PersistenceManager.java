package com.cotiviti.onlineorder.common;

import com.cotiviti.onlineorder.common.constants.MetadataConstants;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

public class PersistenceManager<T extends PreAction> {

    @PersistenceContext
    protected EntityManager em;

    private final Class<T> clazz;

    public PersistenceManager() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.clazz = (Class<T>) genericSuperclass.getActualTypeArguments ()[0];
    }

    public List<T> getAllEntities() {
        try{
            return new ArrayList<>(new LinkedHashSet<T>(makeQueryforGetAllEntity().getResultList()));
        }
        catch (Exception e){
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public void save(T t) {
        t.preAction();
        em.persist ( t );
    }

    public T update(T t) {
        t.preAction();
        return em.merge ( t );
    }

    public T getEntityById(int id) {
        String jqpl = "select entity from " + clazz.getName()
                + " entity where entity.id=:id and entity.activeStatus!=:activeStatus";
        Query query = this.em.createQuery(jqpl);
        query.setParameter("activeStatus", MetadataConstants.INACTIVE);
        query.setParameter("id", id);
        return (T) query.getSingleResult();

    }

    private Query makeQueryforGetAllEntity() {
        String jqpl = "select entity from " + clazz.getName ( ) + " entity where entity.activeStatus!=:activeStatus order by entity.id ";
        Query query = this.em.createQuery ( jqpl );
        query.setParameter ( "activeStatus", MetadataConstants.INACTIVE );
        return query;
    }
}
