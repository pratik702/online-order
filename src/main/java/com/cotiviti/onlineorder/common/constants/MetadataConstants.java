package com.cotiviti.onlineorder.common.constants;

public class MetadataConstants {

    public static final String ACTIVE = "A";
    public static final String INACTIVE = "I";
    public static final String SUCCESS = "SUCCESS";
    public static final String FAILURE = "FAILURE";
}
