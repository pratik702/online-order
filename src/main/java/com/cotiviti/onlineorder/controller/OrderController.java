package com.cotiviti.onlineorder.controller;

import com.cotiviti.onlineorder.entity.DashboardDTO;
import com.cotiviti.onlineorder.entity.Menu;
import com.cotiviti.onlineorder.entity.OrderBook;
import com.cotiviti.onlineorder.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping(value ="/menus", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Menu> getAllMenus(){
        return orderService.getAllMenus();
    }

    @GetMapping(value ="/orderbooks", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OrderBook> getAllOrderBooks(){
        return orderService.getAllOrderBooks();
    }

    @PostMapping(value ="place-order", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity placeOrder(@RequestBody OrderBook orderBook){
        return orderService.placeOrder(orderBook);
    }

    @GetMapping(value ="/dashboard", produces = MediaType.APPLICATION_JSON_VALUE)
    public DashboardDTO getDashboardData(){
        return orderService.getDashboardData();
    }

}
