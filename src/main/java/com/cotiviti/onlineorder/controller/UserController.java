package com.cotiviti.onlineorder.controller;

import com.cotiviti.onlineorder.entity.User;
import com.cotiviti.onlineorder.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllMenus(){
        return "Welcome to Online Order System";
    }

    @PostMapping(value = "/sign-up", produces = MediaType.APPLICATION_JSON_VALUE)
    public String saveNewUser(@RequestBody User user){
        return userService.saveNewUser(user);
    }
}
