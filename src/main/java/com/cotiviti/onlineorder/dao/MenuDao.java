package com.cotiviti.onlineorder.dao;

import com.cotiviti.onlineorder.common.PersistenceManager;
import com.cotiviti.onlineorder.entity.Menu;
import org.springframework.stereotype.Repository;

@Repository
public class MenuDao extends PersistenceManager<Menu> {

}
