package com.cotiviti.onlineorder.dao;

import com.cotiviti.onlineorder.common.PersistenceManager;
import com.cotiviti.onlineorder.common.constants.MetadataConstants;
import com.cotiviti.onlineorder.entity.DashboardDTO;
import com.cotiviti.onlineorder.entity.OrderBook;
import com.cotiviti.onlineorder.entity.TopOrderItemDTO;
import com.cotiviti.onlineorder.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class OrderBookDao extends PersistenceManager<OrderBook>  {

    public List<OrderBook> getAllOrderBooks(){
        try{
            String jpql = "select entity from OrderBook entity where entity.activeStatus =:activeStatus";
            Query query = this.em.createQuery(jpql);
            query.setParameter("activeStatus", MetadataConstants.ACTIVE);
            return (List<OrderBook>) query.getResultList();
        }
        catch(Exception e){
            return new ArrayList<>();
        }
    }

    public TopOrderItemDTO getTopOrderItem(){
        try {
            String sql = "SELECT  TOP 1 MENU_ID, NAME, COUNT(MENU_ID) AS NUM FROM ORDER_ITEM LEFT OUTER JOIN MENU ON ORDER_ITEM.MENU_ID = MENU.ID GROUP BY MENU_ID ORDER BY NUM DESC";
            Query query = this.em.createNativeQuery(sql, "DashboardDTOMapping");
            return (TopOrderItemDTO) query.getSingleResult();
        }
        catch(Exception e){
            return null;
        }
    }

}
