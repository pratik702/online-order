package com.cotiviti.onlineorder.dao;

import com.cotiviti.onlineorder.common.PersistenceManager;
import com.cotiviti.onlineorder.common.constants.MetadataConstants;
import com.cotiviti.onlineorder.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;

@Repository
public class UserDao extends PersistenceManager<User> {

    public User getUserByUserName(String userName){
        String jpql = "select entity from User entity where entity.activeStatus =:activeStatus and entity.userName = :userName";
        Query query = this.em.createQuery(jpql);
        query.setParameter("activeStatus", MetadataConstants.ACTIVE);
        query.setParameter("userName", userName);
        return (User) query.getSingleResult();
    }
}
