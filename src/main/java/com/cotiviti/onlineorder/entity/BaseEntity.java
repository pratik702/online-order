package com.cotiviti.onlineorder.entity;

import com.cotiviti.onlineorder.common.PreAction;
import com.cotiviti.onlineorder.common.constants.MetadataConstants;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@MappedSuperclass
public abstract class BaseEntity extends PreAction implements Serializable {

    @Id
    @Access(AccessType.PROPERTY)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "ACTIVE_STATUS")
    private String activeStatus = MetadataConstants.ACTIVE;

    @Override
    public void preAction() {
        super.preAction();
        if(this.activeStatus == null){
            // make default active;
            this.activeStatus = "A";
        }
    }
}
