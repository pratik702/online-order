package com.cotiviti.onlineorder.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
public class DashboardDTO implements Serializable {

    private Integer numberOfOrders;
    private String mostOrderedItem;
    private Integer mostOrderedItemCount;
    private BigDecimal totalAmountCollected;

}
