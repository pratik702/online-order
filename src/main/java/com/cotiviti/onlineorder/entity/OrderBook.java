package com.cotiviti.onlineorder.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "ORDER_BOOK")
public class OrderBook extends BaseEntity {

    @Column(name = "CUSTOMER_NAME")
    private String customerName;

    @Column(name = "CUSTOMER_PHONE")
    private String customerPhone;

    @OneToMany(mappedBy = "orderBook", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<OrderItem> orderItems;

    @OneToOne(mappedBy = "orderBook", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private TenderDetails tenderDetails;

    @Override
    public void preAction() {
        super.preAction();
        if (orderItems != null){
            orderItems.forEach(orderItem -> {
                orderItem.setOrderBook(this);
            });
        }
        if (tenderDetails != null){
            tenderDetails.setOrderBook(this);
        }
    }
}
