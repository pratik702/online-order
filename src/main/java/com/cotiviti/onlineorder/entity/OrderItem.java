package com.cotiviti.onlineorder.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;


@SqlResultSetMappings({
        @SqlResultSetMapping(name = "DashboardDTOMapping", classes = {
                @ConstructorResult(targetClass = TopOrderItemDTO.class, columns = {
                        @ColumnResult(name = "MENU_ID", type = Integer.class),
                        @ColumnResult(name = "NAME", type = String.class),
                        @ColumnResult(name = "NUM", type = Integer.class)
                })
        })
})
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="ORDER_ITEM")
public class OrderItem extends BaseEntity {

    @Column(name = "QUANTITY")
    private Integer quantity;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "MENU_ID")
    private Menu menu;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_BOOK_ID", nullable = false, updatable = false)
    private OrderBook orderBook;
}
