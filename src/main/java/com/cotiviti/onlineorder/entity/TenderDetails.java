package com.cotiviti.onlineorder.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "TENDER_DETAILS")
public class TenderDetails extends BaseEntity{

    @Column(name = "AMOUNT")
    private BigDecimal amount;

    @Column(name = "PAYMENT_MEDIUM")
    private String paymentMedium;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_BOOK_ID", nullable = false, updatable = false)
    private OrderBook orderBook;
}
