package com.cotiviti.onlineorder.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TopOrderItemDTO {
    Integer menuId;
    String name;
    Integer numberOfOrders;
}
