package com.cotiviti.onlineorder.service;

import com.cotiviti.onlineorder.common.constants.MetadataConstants;
import com.cotiviti.onlineorder.dao.MenuDao;
import com.cotiviti.onlineorder.dao.OrderBookDao;
import com.cotiviti.onlineorder.entity.*;
import com.sun.net.httpserver.Authenticator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

@Transactional
@Service
public class OrderService {

    @Autowired
    private MenuDao menuDao;

    @Autowired
    private OrderBookDao orderBookDao;

    public List<Menu> getAllMenus(){
        List<Menu> menu =  menuDao.getAllEntities();
        if (menu != null && menu.size() > 1) {
            menu.sort(Comparator.comparing(Menu::getName));
        }
        return menu;

    }

    public List<OrderBook> getAllOrderBooks(){
        return orderBookDao.getAllEntities();
    }

    public ResponseEntity placeOrder(OrderBook orderBook){
        try{
            orderBookDao.save(orderBook);
            return new ResponseEntity<Authenticator.Success>(HttpStatus.OK);
        }
        catch(Exception e){
            e.printStackTrace();
            return new ResponseEntity<Error>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public DashboardDTO getDashboardData(){
        HashMap<Integer, Integer> orderItemsMap = new HashMap<>();
        Integer counter = 0;
        List<OrderBook> orderBooks = orderBookDao.getAllOrderBooks();
        BigDecimal totalAmountCollected = new BigDecimal(0.0);
        for (OrderBook orderBook: orderBooks){
            for (OrderItem orderItem: orderBook.getOrderItems()){
                counter = orderItemsMap.get(orderItem.getMenu().getId());
                orderItemsMap.put(orderItem.getMenu().getId(), counter != null? counter+orderItem.getQuantity(): orderItem.getQuantity());
                totalAmountCollected = totalAmountCollected.add(orderItem.getMenu().getPrice());
            }
        }

        DashboardDTO dashboardDTO = new DashboardDTO();
        dashboardDTO.setNumberOfOrders(orderBooks.size());
        dashboardDTO.setTotalAmountCollected(totalAmountCollected);

        Integer maxCount = 0;
        Integer topMenuId = 0;
        for (Integer key : orderItemsMap.keySet()) {
            if (orderItemsMap.get(key) > maxCount){
                maxCount = orderItemsMap.get(key);
                topMenuId = key;
            }
        }
        if (topMenuId != 0){
            Menu topMenuItem = menuDao.getEntityById(topMenuId);
            dashboardDTO.setMostOrderedItemCount(maxCount);
            dashboardDTO.setMostOrderedItem(topMenuItem.getName());
        }
        else{
            dashboardDTO.setMostOrderedItemCount(0);
            dashboardDTO.setMostOrderedItem("N/A");
        }

        return dashboardDTO;
    }

}
