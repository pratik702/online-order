package com.cotiviti.onlineorder.service;

import com.cotiviti.onlineorder.common.constants.MetadataConstants;
import com.cotiviti.onlineorder.dao.UserDao;
import com.cotiviti.onlineorder.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import static java.util.Collections.emptyList;

@Transactional
@Service
public class UserService implements UserDetailsService {

    @Autowired
    UserDao userDao;

    @Autowired
    BCryptPasswordEncoder passwordEncoder;

    public String saveNewUser(User user){
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userDao.save(user);
        return MetadataConstants.SUCCESS;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User user = userDao.getUserByUserName(userName);
        if (user == null){
            throw new UsernameNotFoundException(userName);
        }
        return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(), emptyList());
    }
}
